import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class CoinFlipApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("coin-app.fxml"));
        stage.setTitle("Let's play coin!");
        stage.setScene(new Scene(root, 600, 420));
        stage.show();
    }
}
