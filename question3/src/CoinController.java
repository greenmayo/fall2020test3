import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class CoinController implements Initializable {
    @FXML
    private Button heads = new Button();
    @FXML
    private Button tails = new Button();
    @FXML
    private TextField bet = new TextField();
    @FXML
    private TextField congrats = new TextField();
    @FXML
    private TextField bankAmount = new TextField();
    @FXML
    private int playerBank = 100;

    private int betAmount = 0;
    private boolean choice;
    private boolean randomCoin;
    private Random rd = new Random();

    @FXML
    private void heads(){
        this.choice = true;
        placeBet();
    }

    @FXML
    private void tails(){
        this.choice = false;
        placeBet();
    }

    public void placeBet(){ // here the logic happens
        if(!bet.getText().equals("")) {
            this.betAmount = Integer.parseInt(bet.getText());
            if (betAmount <= 0 || betAmount > playerBank) { // if bet is <=0 or less than player has
                Alert invalidBet = new Alert(Alert.AlertType.ERROR);
                invalidBet.setTitle("Invalid bet!");
                invalidBet.setContentText("Invalid amount: try something reasonable!");
                invalidBet.setHeaderText(null);
                invalidBet.showAndWait();
            } else { // checking the game
                this.randomCoin = rd.nextBoolean();
                if (this.choice == this.randomCoin) { //we set heads to be true, tails to be false
                    this.congrats.setText("You have won with " + ((choice) ? "heads" : "tails")); //if guessed than good
                    this.playerBank += betAmount;
                    this.bankAmount.setText("" + this.playerBank);
                } else {
                    this.congrats.setText("You have lost with " + ((choice) ? "heads" : "tails")); // not guessed, wrong
                    this.playerBank -= betAmount;
                    this.bankAmount.setText("" + this.playerBank);
                }

            }
        }
        else { // sorry for duplicating this piece of code, have no time to actually make it more presentable
            Alert invalidBet = new Alert(Alert.AlertType.ERROR);
            invalidBet.setTitle("Invalid bet!");
            invalidBet.setContentText("Enter an amount please.");
            invalidBet.setHeaderText(null);
            invalidBet.showAndWait();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) { // initialising the game with bank of the player
        this.bankAmount.setText(""+this.playerBank);

    }
}
