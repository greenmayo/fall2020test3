public class Recursion {

    public int recursiveCount(int[] numbers, int n) {
        if (n < numbers.length) {
            if (numbers[n] >= 20 && n % 2 == 1) {
                return recursiveCount(numbers, n + 1) + 1;
            } else {
                return recursiveCount(numbers, n + 1);
            }
        }
        return 0;
    }
}
